import React from "react";
import NotFound from './containers/NotFound';
import Login from './containers/Login'
import Dashboard from './containers/Dashboard'
import LayoutPage from './containers/Layout'
import TablePage from './containers/Table'
import FormPage from './containers/Form'
import CarrerPage from './containers/Career'
import IntegrationPage from './containers/Integration'
import { Route, Switch, useLocation } from "react-router-dom";
import MainLayout from './layouts/Main';

function Router(props) {
  const location = useLocation()
  const currentRoute = location.pathname
  return (
    <>
    {currentRoute==='/' ?
        <Switch>
          <Route path="/" exact component={Login}></Route>
        </Switch>
      : 
      <MainLayout>
        <Switch>
          <Route path="/dashboard" exact component={Dashboard}></Route>
          <Route path="/career" exact component={CarrerPage}></Route>
          <Route path="/layout" exact component={LayoutPage}></Route>
          <Route path="/table" exact component={TablePage}></Route>
          <Route path="/form" exact component={FormPage}></Route>
          <Route path="/axiospage" exact component={IntegrationPage}></Route>
          <Route component={NotFound}></Route>
        </Switch>
      </MainLayout >
    }
    </>
  );
}

export default Router;