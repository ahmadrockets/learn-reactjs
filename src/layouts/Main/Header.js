import React from "react";
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import AccountCircle from '@material-ui/icons/AccountCircle';
import NotificationsIcon from '@material-ui/icons/Notifications';
import TimelineIcon from '@material-ui/icons/Timeline';
import AppBar from '@material-ui/core/AppBar';
import LogoCIMB from '../../assets/images/logo_cimb.png';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    background: '#fafafa'
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  headerLogo: {
    width: '150px',
    float: 'left'
  }
}));

function Header({open, onClick}) {
  const classes = useStyles();
  return (
    <>
    <AppBar
      position="fixed"
      className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
      })}
    >
      <Toolbar>
        <IconButton
          aria-label="open drawer"
          onClick={onClick}
          edge="start"
          className={clsx(classes.menuButton, {
              [classes.hide]: open,
          })}
        >
        <KeyboardArrowRight />
        </IconButton>
        <Grid item>
          <Typography variant="h6" noWrap>
            <img alt="Logo CIMB" className={classes.headerLogo} src={LogoCIMB} />
          </Typography>
        </Grid>
        <Grid item xs />
        <Grid item>
          <IconButton>
              <TimelineIcon />
          </IconButton>
          <IconButton>
              <NotificationsIcon />
          </IconButton>
          <IconButton>
            <AccountCircle />
          </IconButton>
        </Grid>
      </Toolbar>
    </AppBar>
    </>
  )
}

export default Header;