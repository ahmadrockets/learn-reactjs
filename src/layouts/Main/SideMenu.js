import React from "react";
import clsx from 'clsx';
import { Link } from "react-router-dom";
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import SpeedIcon from '@material-ui/icons/Speed';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ViewColumnIcon from '@material-ui/icons/ViewColumn';
import GridOnIcon from '@material-ui/icons/GridOn';
import AccountIcon from '@material-ui/icons/AccountCircle';
import BorderColorIcon from '@material-ui/icons/BorderColor';
import HttpIcon from '@material-ui/icons/Http';
import Divider from '@material-ui/core/Divider';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    menuItem:{
        textDecoration:'none',
        color: theme.palette.text.primary
    }
}));

const listMenu = [
    { title: "Dashboard", icon: <SpeedIcon />, link: '/dashboard' },
    { title: "Career", icon: <AccountIcon />, link: '/career' },
    { title: "Layout", icon: <ViewColumnIcon />, link: '/layout' },
    { title: "Table", icon: <GridOnIcon />, link: '/table' },
    { title: "Form", icon: <BorderColorIcon />, link: '/form' },
    { title: "Sample Axios", icon: <HttpIcon />, link: '/axiospage' },
]

function SideMenu({onClick, open}) {
    const classes = useStyles();
    const theme = useTheme();
    return(
        <Drawer
            variant="permanent"
            className={clsx(classes.drawer, {
                [classes.drawerOpen]: open,
                [classes.drawerClose]: !open,
            })}
            classes={{
                paper: clsx({
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                }),
            }}
        >
            <div className={classes.toolbar}>
                <IconButton onClick={onClick}>
                    {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                </IconButton>
            </div>
            <Divider />
            <List>
                {listMenu.map((row, index) => (
                    <Link to={row.link} key={index} className={classes.menuItem}>
                        <ListItem button>
                            <ListItemIcon>{row.icon}</ListItemIcon>
                            <ListItemText primary={row.title} />
                        </ListItem>
                    </Link>
                ))}
            </List>
        </Drawer>
    )
}

export default SideMenu;