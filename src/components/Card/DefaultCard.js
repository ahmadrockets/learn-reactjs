import React from "react";
import {Card } from "antd";

export default function DefaultCard(props){
  const cardHeadStyle = { background: props.headercolor ? props.headercolor : '#0092ff', color: '#fff' }
  return (
    <>
      <Card title={props.title} headStyle={cardHeadStyle}>
        {props.children}
      </Card>
    </>
  )
}