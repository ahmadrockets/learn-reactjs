import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Row, Col, Divider, Form, Input, InputNumber, Button, notification, Space } from "antd";
import LogoIst from "../../assets/images/logo-ist.png";
import ProfilPic from "../../assets/images/photo.jpg";
const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: '#fff',
  },
  headerBg: {
    position: 'relative',
    backgroundColor: '#f1592a',
    minHeight: '200px',
  },
  logoIST: {
    height: '100px',
    float: 'right',
    marginRight: '30px'
  },
  profilPic: {
    position: 'absolute',
    height: '200px',
    width: '200px',
    backgroundColor: '#fff',
    borderRadius: '100px',
    bottom: 0,
    marginLeft: '50px',
    marginBottom: '-100px',
    padding: '8px'
  },
  profilPicImg: {
    height: '100%',
    width: '100%',
    borderRadius: '50%'
  }
}));
function HeaderCareer() {
  const classes = useStyles()
  return (
    <>
      <div className={classes.headerBg} span={24}>
        <img alt="Logo IST" className={classes.logoIST} src={LogoIst} />
        <div className={classes.profilPic}>
          <img alt="Profil Picture" className={classes.profilPicImg} src={ProfilPic} />
        </div>
      </div>
    </>
  );
}

export default HeaderCareer;