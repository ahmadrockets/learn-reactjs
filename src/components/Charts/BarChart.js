import React from "react";
import { Bar } from '@ant-design/charts';

export default function LineChart({ data, xfield, yfield }) {
  var config = {
    data: data,
    xField: xfield,
    yField: yfield,
    seriesField: yfield,
    legend: { position: 'top-left' },
  };
  return (
    <>
      <Bar {...config} />
    </>
  )
}