import React from "react";
import { Pie } from '@ant-design/charts';

export default function LineChart({ data, angleField, colorField }) {
  var config = {
    appendPadding: 10,
    data: data,
    angleField: angleField,
    colorField: colorField,
    radius: 0.9,
    label: {
      type: 'outer',
      content: '{percentage}',
    },
    interactions: [{ type: 'element-active' }],
  };
  return (
    <>
      <Pie {...config} />
    </>
  )
}