import React, {useState} from "react";
import { useHistory } from "react-router-dom";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import IconEmail from '../../../assets/icons/icon_email.svg';
import IconLock from '../../../assets/icons/icon_lock.svg';
import IconEye from '../../../assets/icons/icon_eye.svg';
import IconEyeInvisible from '../../../assets/icons/icon_eye_invisible.svg';
import './Login.css';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function FormLogin() {
    const [email, setEmail] = useState('');
    const [isEmailTrue, setEmailtrue] = useState(1);
    const [styleEmail, setStyleEmail] = useState('input');
    const [password, setPassword] = useState('');
    const [isPassTrue, setPasstrue] = useState(1);
    const [isEditPass, setEditPass] = useState(0);
    const [stylePass, setStylePass] = useState('input');
    const [isShowPass, setShowPass] = useState(0);
    const [typePass, setTypePass] = useState('password');
    const emailtrue     = 'admin@admin.com';
    const passwordtrue  = '123456';
    const history = useHistory();
    const [open, setOpen] = useState(false);
    const [alert, setAlert] = useState({mode:'success', text:'Login success'});
    const Login = (e) => {
        e.preventDefault()
        if (email!==emailtrue) {
            setEmailtrue(0)
            setOpen(true)
            setAlert({ mode: 'error', text: 'Login Failed' })
            return false
        }
        if (password !== passwordtrue) {
            setPasstrue(0)
            setStylePass('input-incorrect')
            setOpen(true)
            setAlert({ mode: 'error', text: 'Login Failed' })
            return false
        }
        setPasstrue(1)
        setStylePass('input')
        setAlert({ mode: 'success', text: 'Login Success' })
        setOpen(true)
        setTimeout(() => {
            history.push('/dashboard')
        }, 2000);
    }
    const handleKeyupEmail = (e) =>{
        setEmail(e.target.value)
        const emailregex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        if (emailregex.test(e.target.value)) {
            setEmailtrue(1)
            setStyleEmail('input')
        }else{
            setEmailtrue(0)
            setStyleEmail('input-incorrect')
        }
    }
    const handleKeyupPass = (e) => {
        const valpass = e.target.value
        setPassword(valpass)
        if (valpass.length>0) {
            setStylePass('input')
            setEditPass(1)
        }else{
            setEditPass(0)
        }
    }
    const showPass = ()=>{
        if (isShowPass===0) {
            setShowPass(1)
            setTypePass('text')
        }else{
            setShowPass(0)
            setTypePass('password')
        }
    }
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };
    return (
        <>
            <div className="card-login">
                <span className="title">Welcome To</span><br />
                <span className="sub-title">Sistem Informasi ATM Business</span>
                <form onSubmit={Login} className="form-login">
                    <div className="input-group">
                        <input className={styleEmail} value={email} placeholder="Username/email" type="text" onChange={handleKeyupEmail} onKeyUp={handleKeyupEmail} />
                        <span className="inputicon"><img alt="Email" src={IconEmail} /></span>
                        { isEmailTrue===0 &&
                            <span  className="span-alert">Incorrect email</span>
                        }
                    </div>
                    <div className="input-group">
                        <input className={stylePass} value={password} placeholder="Password" onChange={handleKeyupPass} type={typePass} onKeyUp={handleKeyupPass}/>
                        {isEditPass===0 && <span className="inputicon"><img alt="Lock" src={IconLock} /></span>}
                        {(isEditPass === 1 && isShowPass===0) && 
                            <span onClick={showPass} className="inputicon event"><img alt="Show Password" src={IconEyeInvisible} /></span>
                        }
                        {(isEditPass === 1 && isShowPass===1) &&
                            <span onClick={showPass} className="inputicon event"><img alt="Hidden Password" src={IconEye} /></span>
                        }
                        { isPassTrue===0 &&
                            <span className="span-alert">Incorrect password</span>
                        }
                    </div>
                    <div className="form-footer">
                        <a href="!#" className="forgot-password">Forgot Password Merge</a>
                        <button className="button-login" type="submit">Login</button>
                    </div>
                </form>
            </div>
            <Snackbar 
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }} 
                open={open} 
                autoHideDuration={6000} 
                onClose={handleClose}
            >
                <Alert onClose={handleClose} severity={alert.mode}>
                    {alert.text}
                </Alert>
            </Snackbar>
        </>
    );
}

export default FormLogin;
