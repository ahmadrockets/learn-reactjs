import React, {useState, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { Row, Col, Table, Space, Button, Modal} from "antd";
import DefaultCard from "../../components/Card/DefaultCard";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
  },
  marginBottom:{
    marginBottom: '10px'
  }
}));

export default function Integration() {
  const classes = useStyles();
  const [isloading, setIsloading] = useState(false)
  const [data, setData] = useState([])
  const [detailRecord, setDetailRecord] = useState({})
  const [modalDetail, setModalDetail] = useState(false)
  
  const columns = [
    {
      title: 'No',
      dataIndex: 'userId',
      key: 'userId',
      width: 100,
      sorter: (a, b) => a.userId.length - b.userId.length,
    },
    {
      title: 'First Name',
      dataIndex: 'firstName',
      key: 'firstName',
      sorter: (a, b) => a.firstName.length - b.firstName.length,
    },
    {
      title: 'Last Name',
      dataIndex: 'lastName',
      key: 'lastName',
    },
    {
      title: 'Phone Number',
      dataIndex: 'phoneNumber',
      key: 'phoneNumber',
    },
    {
      title: 'Action',
      key: 'action',
      width: 200,
      render: (text, record) => (
        <Space size="middle">
          <Button type="secondary" onClick={() => detailData(record)}>Detail</Button>
        </Space>
      ),
    },
  ];
  
  const getData = (e) => {
    setIsloading(true)
    axios.get('https://learnreactjs.getsandbox.com/api/users')
      .then(res => {
        setData(res.data.data)
        setIsloading(false)
      }).catch(err => {
        setIsloading(false)
        console.log(err)
      })
  }
  const detailData = (record) =>{
    setModalDetail(true)
    setDetailRecord(record)
  }
  useEffect(() => {
    getData()
  }, [])
  return (
    <>
      <Grid item xs={12}>
        <Paper className={classes.paper}>
          <center><h2>Axios Page</h2></center>
          <Row gutter={16}>
            <Col className="gutter-row" lg={24} xs={24} sm={24}>
              <DefaultCard title="Sample Axios Page with Sandbox API" headercolor="#4caf50">
                <Space size="middle" className={classes.marginBottom}>
                  <Button type="primary" onClick={getData}>Reload Data</Button>
                </Space>
                <br />
                <Table loading={isloading} columns={columns} dataSource={data} rowKey="userId" />
              </DefaultCard>
            </Col>
          </Row>
        </Paper>
      </Grid>
      <Modal
        title="Detail Data"
        centered
        footer={[
          <Button type="primary" onClick={() => setModalDetail(false)}>
            Close
            </Button>,
        ]}
        visible={modalDetail}
      >
        <Row>
          <Col span={8}>Name</Col>
          <Col span={16}>: {detailRecord.firstName + ' ' + detailRecord.lastName}</Col>
        </Row>
        <Row>
          <Col span={8}>Phone Number</Col>
          <Col span={16}>: {detailRecord.phoneNumber}</Col>
        </Row>
        <Row>
          <Col span={8}>Email Address</Col>
          <Col span={16}>: {detailRecord.emailAddress}</Col>
        </Row>
      </Modal>
    </>
  );
}