import React, {useState} from 'react';
import { useHistory } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import BanIcon from '@material-ui/icons/NotInterested';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
  }
}));
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function Layout() {
  const [firstName, setFirstName] = useState("")
  const [lastName, setLastName] = useState("")
  const [isErrorName, setIsErrorName] = useState(0)
  const [age, setAge] = useState("")
  const [isErrorAge, setIsErrorAge] = useState(0)
  const classes = useStyles()
  const history = useHistory()
  const [alert, setAlert] = useState({ mode: 'success', text: 'Data saved' })
  const [open, setOpen] = useState(false);

  const handleFirstName = (e) =>{
    let val = e.target.value
    setFirstName(val)
    if(val.length>0 && val.toLowerCase()===lastName.toLowerCase()){
      setIsErrorName(1)
    }else{
      setIsErrorName(0)
    }
  }
  const handleLastName = (e) =>{
    let val = e.target.value
    setLastName(val)
    if(val.length>0 && val.toLowerCase()===firstName.toLowerCase()){
      setIsErrorName(1)
    }else{
      setIsErrorName(0)
    }
  }
  const handleAge = (e) =>{
    let val = e.target.value
    if (!isNaN(val)) {
      setAge(val)
    }
  }

  const reset = () =>{
    setFirstName("")
    setLastName("")
    setIsErrorName(0)
    setAge("")
  }
  const save = () =>{
    reset()
    setOpen(true)
    setTimeout(() => {
      history.push('/table')
    }, 1000);
  }
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };
  return (
    <>
      <Grid item xs={12}>
        <Paper className={classes.paper}>
          <center><h2>Form Page</h2></center>
          <Grid container spacing={3}>
            <Grid item xs={2}>
            </Grid>
            <Grid item xs={8}>
              <form className={classes.root} noValidate autoComplete="off">
                <div >
                <TextField
                  error={isErrorName===0 ? false : true}
                  value={firstName}
                  id="outlined-full-width"
                  label="First Name"
                  style={{ margin: 8 }}
                  placeholder="First Name"
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  onChange={handleFirstName}
                  onKeyUp={handleFirstName}
                  helperText={isErrorName===0 ? "" : "First Name and Last Name must be different"}
                />
                <TextField
                  error={isErrorName===0 ? false : true}
                  id="outlined-full-width"
                  label="Last Name"
                  value={lastName}
                  style={{ margin: 8 }}
                  placeholder="Last Name"
                  fullWidth
                  margin="normal"
                  onChange={handleLastName}
                  onKeyUp={handleLastName}
                  variant="outlined"
                  helperText={isErrorName===0 ? "" : "First Name and Last Name must be different"}
                />
                <TextField
                  id="outlined-full-width"
                  label="Age"
                  value={age}
                  error={isErrorAge===0 ? false : true}
                  style={{ margin: 8 }}
                  placeholder="Age"
                  fullWidth
                  margin="normal"
                  onChange={handleAge}
                  onKeyUp={handleAge}
                  variant="outlined"
                  helperText="Age must be a number"
                />
                <Button disabled={isErrorName === 0 ? false : (isErrorAge===0 ? true : false)} onClick={save} variant="contained" color="primary" style={{ margin: 8 }} startIcon={<SaveIcon />}>
                  Save
                </Button>
                <Button onClick={reset} variant="contained" color="secondary" style={{ margin: 8 }} startIcon={<BanIcon />}>
                  Reset
                </Button>
                </div> 
              </form>
            </Grid>
            <Grid item xs={2}>
              <Snackbar
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'right',
                }}
                open={open}
                autoHideDuration={6000}
                onClose={handleClose}
              >
                <Alert onClose={handleClose} severity={alert.mode}>
                  {alert.text}
                </Alert>
              </Snackbar>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </>
  );
}