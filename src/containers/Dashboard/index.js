import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { Row, Col, Divider, Form, Input, InputNumber, Button, notification, Space} from "antd";
import DefaultCard from "../../components/Card/DefaultCard";
import LineChart from '../../components/Charts/LineChart';
import PieChart from '../../components/Charts/PieChart';
import BarChart from '../../components/Charts/BarChart';
import { ChartData } from "../../helpers/DataDummy/ChartData";
import { Select } from 'antd';
const { Option } = Select;
const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
  }
}));
const openNotificationWithIcon = type => {
  notification[type]({
    message: 'Notification Title',
    description:
      'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
    placement: 'bottomRight'
  });
};

export default function Dashboard() {
  const data = ChartData
  const classes = useStyles()
  return (
    <>
      <Grid item xs={12}>
        <Paper className={classes.paper}>
          <center><h2>Dashboard Page</h2></center>
          <Divider orientation="left">Grid and Card from antd</Divider>
          <Row gutter={16}>
            <Col className="gutter-row" lg={6} xs={24} sm={12}>
              <DefaultCard title="Card 1" >
                Child from Card 1
              </DefaultCard>
            </Col>
            <Col className="gutter-row" lg={6} xs={24} sm={12}>
              <DefaultCard title="Card 2" headercolor="#4caf50">
                Child from Card 2
              </DefaultCard>
            </Col>
            <Col className="gutter-row" lg={6} xs={24} sm={12}>
              <DefaultCard title="Card 3" headercolor="#ff9800">
                Child from Card 3
              </DefaultCard>
            </Col>
            <Col className="gutter-row" lg={6} xs={24} sm={12}>
              <DefaultCard title="Card 4" headercolor="#e91e63">
                Child from Card 4
              </DefaultCard>
            </Col>
          </Row>
          <Divider orientation="left">Simple Charts from antd</Divider>
          <Row gutter={16}>
            <Col className="gutter-row" lg={8} xs={24} sm={12}>
              <DefaultCard title="Line Chart" headercolor="#e91e63">
                <LineChart data={data} xfield="year" yfield="value" />
              </DefaultCard>
            </Col>
            <Col className="gutter-row" lg={8} xs={24} sm={12}>
              <DefaultCard title="Pie Chart" headercolor="#e91e63">
                <PieChart data={data} angleField="value" colorField="year" />
              </DefaultCard>
            </Col>
            <Col className="gutter-row" lg={8} xs={24} sm={12}>
              <DefaultCard title="Bar Chart" headercolor="#e91e63">
                <BarChart data={data} xfield="value" yfield="year" />
              </DefaultCard>
            </Col>
          </Row>
          <Divider orientation="left">Form, Button and Notification from antd</Divider>
          <Row gutter={16}>
            <Col className="gutter-row" lg={12} sm={12} xs={24}>
              <DefaultCard title="Simple Form" >
                <Form
                  name="basic"
                >
                  <Row gutter={16}>
                    <Col span={6}>First Name</Col>
                    <Col span={18}>
                      <Form.Item
                        name="firstname"
                        rules={[{ required: true, message: 'Please input your first name!' }]}
                      >
                        <Input />
                      </Form.Item>  
                    </Col>
                  </Row>
                  <Row gutter={16}>
                    <Col span={6}>Last Name</Col>
                    <Col span={18}>
                      <Form.Item
                        name="lastname"
                      >
                        <Input />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={16}>
                    <Col span={6}>Select</Col>
                    <Col span={18}>
                      <Form.Item
                        name="person"
                      >
                        <Select
                          showSearch
                          style={{ width: '100%' }}
                          placeholder="Select a person"
                          optionFilterProp="children"
                          filterOption={(input, option) =>
                            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                          }
                        >
                          <Option value="jack">Jack</Option>
                          <Option value="lucy">Lucy</Option>
                          <Option value="tom">Tom</Option>
                        </Select>
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={16}>
                    <Col span={6}>Age</Col>
                    <Col span={18}>
                      <Form.Item
                        name="age"
                      >
                        <InputNumber min={0} style={{ width: '100%' }} />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={24} >
                      <Space>
                        <Button type="primary">Button 1</Button>
                        <Button type="primary" danger>Button 2</Button>
                      </Space>
                    </Col>
                  </Row>
                </Form>
              </DefaultCard>
            </Col>
            <Col className="gutter-row" lg={12} sm={12} xs={24}>
              <DefaultCard title="Simple Notification" headercolor="#ff9800">
                <Space>
                  <Button onClick={() => openNotificationWithIcon('success')}>Success</Button>
                  <Button onClick={() => openNotificationWithIcon('info')}>Info</Button>
                  <Button onClick={() => openNotificationWithIcon('warning')}>Warning</Button>
                  <Button onClick={() => openNotificationWithIcon('error')}>Error</Button>
                </Space>
              </DefaultCard>
            </Col>
          </Row>
        </Paper>
      </Grid>
    </>
  );
}