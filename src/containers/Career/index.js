import React, {useState, useRef} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Form, Input, Row, Col, Button } from 'antd';
import HeaderCareer from "../../components/HeaderCareer";
import { SaveOutlined, PlusOutlined, FileTextOutlined } from '@ant-design/icons';
const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor:'#fff',
  },
  formApply: {
    marginTop: '100px',
    padding: '20px',
  },
  rightButton:{
    fontSize:'14px',
    fontWeight: 'bolder',
    float:'right',
    marginTop: '-15px'
  },
  leftButton:{
    fontSize:'16px',
    fontWeight: 'bolder',
    float:'left',
  }
}));
export default function Dashboard() {
  const classes = useStyles();
  const inputFile = useRef(null);
  const [form] = Form.useForm();
  const [fileUpload, setFileUpload] = useState([])
  const [fileName, setFileName] = useState(' ')
  function upload(event) {
    event.preventDefault();
    const file = fileUpload;
    let name = ''
    file.push(event.target.files[0]);
    file.map((data, index) => {
      if (index === 0) {
        name = name + data.name
      } else {
        name = name + ', ' + data.name
      }
    })
    setFileUpload(file);
    setFileName(name)
  }
  const clickUpload = () =>{
    inputFile.current.click();
  }
  return (
    <div className={classes.root}>
      <HeaderCareer />
      <Form
        form={form}
        layout="vertical"
        className={classes.formApply}
      >
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="Nama Depan">
              <Input size="large" placeholder="Isikan Nama Depan" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Nama Belakang">
              <Input size="large" placeholder="Isikan Nama Belakang" />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={24}>
            <Form.Item label="Posisi Saat Ini">
              <Input size="large" placeholder="Isikan Posisi Saat Ini" />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={24}>
            <Form.Item label="Pendidikan">
              <Input size="large" placeholder="Isikan Pendidikan" />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={24}>
            <a className={classes.rightButton}>
              <PlusOutlined /> Pendidikan
            </a>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={24}>
            <Form.Item label="Lokasi">
              <Input size="large" placeholder="Isikan Lokasi" />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={24}>
            <Form.Item label="Industri">
              <Input size="large" placeholder="Isikan Industri" />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={24}>
            <Form.Item label="Info Kontak">
              <Input size="large" placeholder="Isikan Info Kontak" />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={24}>
            <a className={classes.rightButton}>
              <PlusOutlined /> Info Kontak
            </a>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={24}>
            <a className={classes.leftButton} onClick={clickUpload}>
              <FileTextOutlined /> Lampiran CV
              <input
                id="upload-file"
                type="file"
                accept=".pdf"
                ref={inputFile}
                style={{display:'none'}}
                onChange={upload}
              />
            </a>
            &nbsp; {fileName}
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={24}>
            <Button style={{float:'right'}} type="primary" icon={<SaveOutlined />} size="large">
              Save
            </Button>
          </Col>
        </Row>
      </Form>
    </div>
  );
}