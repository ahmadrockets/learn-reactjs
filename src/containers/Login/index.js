import React from "react";
import FormLogin from '../../components/Auth/Login/FormLogin';
import './Login.css';
import Footer from '../../layouts/Footer';
import LogoCIMB from '../../assets/images/logo_cimb.png';

function Login() {
    return (
        <>
            <div className="div-form-login">
                <img alt="Logo CIMB" className="logo-cimb" src={LogoCIMB} />
                <FormLogin />
                <Footer />
            </div> 
        </>
    );
}

export default Login;